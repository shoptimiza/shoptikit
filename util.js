'use strict';

export function deepCopy(input) {
    return JSON.parse(JSON.stringify(input));
}
export function mountTag(id, html) {
    var element = document.getElementById(id);
    if (element) {
        element.innerHTML = html;
        return element;
    }
}

export function hasClass(element, className) {
    return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
}
