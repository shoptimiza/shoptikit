/* global SHOPTIMIZA_API */
'use strict';
import {
    loadAsyncJSONP
} from './http-client';

export default function loader(apiKey, productId, mappingUrl, callback) {
    loadAsyncJSONP(SHOPTIMIZA_API + apiKey + '/' + productId, callback);
}
