import replace from 'rollup-plugin-replace';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import builtins from 'rollup-plugin-node-builtins';
const name = 'shoptimiza';
const shoptimizaApi = "/v1/re/";
//const shoptimizaApi =  "https://api.shoptimiza.com/v1/re/";
export default {
    input: 'sample.js',
    //        sourcemap: 'inline',
    plugins: [
        resolve({
            jsnext: true,
            main: true,
            browser: true,
            module: true,
        }),
        builtins(),
        commonjs()
    ],
    output: {
        intro: `var SHOPTIMIZA_API="${shoptimizaApi}", EXPORT_NAME="${name}";`,
        name,
        file: 'sample-output.js',
        format: 'iife',
    }
};
