var shoptimiza = (function () {
    'use strict';

    var SHOPTIMIZA_API="/v1/re/", EXPORT_NAME="shoptimiza";

    function post(url, data, callback) {
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                try {
                    return callback(null, request.responseText);
                } catch (e) {
                    return callback(e);
                }
            } else {
                return callback(new Error(request.status));
            }
        };
        request.send('data=' + encodeURIComponent(JSON.stringify(data)));
    }

    function cacheBuster() {
        return '&r=' + (Math.random() + '').slice(2, 8);
    }

    function loadAsyncJSONP(url, callback) {
        var element = document.createElement('script');
        if (typeof callback === 'function') {
            callback = callback.name;
        }
        if (!callback) {
            throw new Error('callback should be a named function or a function name');
        }
        if (url.indexOf('?') > -1) {
            url += '&callback=' + callback;
        } else {
            url += '?callback=' + callback;
        }
        url += cacheBuster();
        element.async = 1;
        element.src = url;
        document.body.appendChild(element);
    }

    function mountTag(id, html) {
        var element = document.getElementById(id);
        if (element) {
            element.innerHTML = html;
            return element;
        }
    }

    function hasClass(element, className) {
        return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
    }

    /* global SHOPTIMIZA_API */

    function loader(apiKey, productId, mappingUrl, callback) {
        loadAsyncJSONP(SHOPTIMIZA_API + apiKey + '/' + productId, callback);
    }

    /* global EXPORT_NAME */

    function shoptimizaLoader(options) {
        function visit(url) {
            return function () {
                document.location.href = url;
            }
        }

        function notify(url, e) {
            var time = (new Date().getTime()),
                finalUrl = '/beacon?r=' + time + '&url=' + encodeURIComponent(url);
            e.preventDefault();
            var img = new Image();
            img.onerror = img.onload = visit(url);
            img.src = finalUrl;
        }

        function catchClick(e) {
            var current = e.target,
                productId;
            while (current) {
                if (current.getAttribute && (productId = current.getAttribute('data-id-product'))) {
                    notify(current.getAttribute('href'), e);
                    return;
                }
                current = current.parentNode;
            }
        }

        function catchBundleAddToCart(items) {
            return function (e) {
                e.preventDefault();
                console.log('+ items to cart', items);
            }
        }

        function cb(items) {
            post(options.mappingUrl, items, function (err, res) {
                var items = JSON.parse(res);
                post(options.alsoBoughtHTML, items, function (err, html) {
                    var container = mountTag('also-bought', html);
                    if (container) {
                        container.addEventListener('click', catchClick);
                    }
                });
                post(options.boughtTogetherHTML, items, function (err, html) {
                    var i, elements, container = mountTag('bought-together', html);
                    if (!container) {
                        return;
                    }
                    elements = container.getElementsByTagName('a');
                    for (i = 0; i < elements.length; i++) {
                        if (hasClass(elements[i], 'shoptimiza-bundle-buy-button')) {
                            elements[i].addEventListener('click', catchBundleAddToCart(items.bundles));
                            return;
                        }
                    }
                });
            });
        }
        shoptimizaLoader.cb = cb;
        loader(options.apiKey, options.productId, options.mappingUrl, EXPORT_NAME + '.cb');
    }

    return shoptimizaLoader;

}());
