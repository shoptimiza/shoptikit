/* global EXPORT_NAME */
'use strict';
import {
    post,
    loader,
    mountTag,
    hasClass
} from '../..';

function shoptimizaLoader(options) {
    function visit(url) {
        return function () {
            document.location.href = url;
        }
    }

    function notify(url, e) {
        var time = (new Date().getTime()),
            finalUrl = '/beacon?r=' + time + '&url=' + encodeURIComponent(url);
        if (false && navigator && navigator.sendBeacon) {
            navigator.sendBeacon(finalUrl);
            return;
        }
        e.preventDefault();
        var img = new Image();
        var visitUrl = visit(url);
        img.onerror = img.onload = visit(url);
        img.src = finalUrl;
    }

    function catchClick(e) {
        var current = e.target,
            productId;
        while (current) {
            if (current.getAttribute && (productId = current.getAttribute('data-id-product'))) {
                notify(current.getAttribute('href'), e);
                return;
            }
            current = current.parentNode;
        }
    }

    function catchBundleAddToCart(items) {
        return function (e) {
            e.preventDefault();
            console.log('+ items to cart', items);
        }
    }

    function cb(items) {
        post(options.mappingUrl, items, function (err, res) {
            var items = JSON.parse(res);
            post(options.alsoBoughtHTML, items, function (err, html) {
                var container = mountTag('also-bought', html);
                if (container) {
                    container.addEventListener('click', catchClick);
                }
            });
            post(options.boughtTogetherHTML, items, function (err, html) {
                var i, elements, container = mountTag('bought-together', html);
                if (!container) {
                    return;
                }
                elements = container.getElementsByTagName('a');
                for (i = 0; i < elements.length; i++) {
                    if (hasClass(elements[i], 'shoptimiza-bundle-buy-button')) {
                        elements[i].addEventListener('click', catchBundleAddToCart(items.bundles));
                        return;
                    }
                }
            });
        });
    }
    shoptimizaLoader.cb = cb;
    loader(options.apiKey, options.productId, options.mappingUrl, EXPORT_NAME + '.cb');
}
export default shoptimizaLoader;
