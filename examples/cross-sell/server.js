/* eslint-env node*/
'use strict';
var app;
const express = require('express'),
    path = require('path'),
    HTTP_PORT = process.env.HTTP_PORT || 3000,
    bodyParser = require('body-parser');
app = express();
app.use(bodyParser.urlencoded({
    extended: false
}));
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'sample.html'));
});
app.get('/sample-output.js', function (req, res) {
    res.sendFile(path.join(__dirname, 'sample-output.js'));
});
app.post('/map', function (req, res) {
    /*
    input { alsoBought: [ '425', '336', '115', '144', '531', '572' ],
      boughtTogether: [],
      key: '53' }

    */
    var input = JSON.parse(req.body.data);
    var output = {
        items: {},
        bundles: input.boughtTogether
    };
    var items = input.alsoBought.concat(input.boughtTogether);
    items.forEach(function (x) {
        output.items[x] = {
            id: x,
            thumbs: {},
            price: '1€',
            name: 'Some product ' + x,
            rating: 0,
            url: '/product/' + x,
        };
    });
    res.json(output);
});
app.get('/v1/re/some/1', function (req, res) {
    res.jsonp({
        'boughtTogether': ['1', '5', '6'],
        'alsoBought': ['3', '2'],
        'key': '1'
    });
});
app.get('/beacon', function (req, res) {
    console.log('GET beacon', req.originalUrl, decodeURIComponent(req.query.url));
    res.end();
});
app.post('/beacon', function (req, res) {
    console.log('POST beacon', req.originalUrl, decodeURIComponent(req.query.url));
    res.end();
});
app.get('/product/:id', function (req, res) {
    res.end('product:' + req.params.id);
});
app.post('/also-bought-html', function (req, res) {
    var input = JSON.parse(req.body.data);
    var html = '<div><ul>';
    for (let id in input.items) {
        let item = input.items[id];
        html += `<li><a href="${item.url}" data-id-product="${id}"><h4>${item.name}</h4></a></li>`;
    }
    html += '</ul></div>';
    res.send(html);
});
app.post('/bought-together-html', function (req, res) {
    var input = JSON.parse(req.body.data);
    var html = '<div><p>Bundle</p><ul>';
    for (let id in input.items) {
        if (input.bundles && !!~input.bundles.indexOf(id)) {
            let item = input.items[id];
            html += `<li><a href="${item.url}" data-id-product="${id}"><h4>${item.name}</h4></a></li>`;
        }
    }
    html += '</ul>';
    html += '<a class="shoptimiza-bundle-buy-button" href="#">Buy</a>';
    html += '</div>';
    res.send(html);
});
app.listen(HTTP_PORT, function (err) {
    if (err) {
        console.error(`can not listen at port ${HTTP_PORT}`);
        process.exit(1);
    }
    console.error(`listening at ${HTTP_PORT}`);
});
