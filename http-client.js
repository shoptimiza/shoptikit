'use strict';

export function post(url, data, callback) {
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            try {
                return callback(null, request.responseText);
            } catch (e) {
                return callback(e);
            }
        } else {
            return callback(new Error(request.status));
        }
    };
    request.send('data=' + encodeURIComponent(JSON.stringify(data)));
}

function cacheBuster() {
    return '&r=' + (Math.random() + '').slice(2, 8);
}

export function loadAsyncJSONP(url, callback) {
    var element = document.createElement('script');
    if (typeof callback === 'function') {
        callback = callback.name;
    }
    if (!callback) {
        throw new Error('callback should be a named function or a function name');
    }
    if (url.indexOf('?') > -1) {
        url += '&callback=' + callback;
    } else {
        url += '?callback=' + callback;
    }
    url += cacheBuster();
    element.async = 1;
    element.src = url;
    document.body.appendChild(element);
}
